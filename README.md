Demo Project:
Run Nexus on Droplet and Publish Artifact to Nexus
Technologies used:
Nexus, DigitalOcean, Linux, Java, Gradle, Maven
Project Description:
Install and configure Nexus from scratch on a cloud server
Create new User on Nexus with relevant permissions
Java Gradle Project: Build Jar & Upload to Nexus
Java Maven Project: Build Jar & Upload to Nexus
===============================================

Steps:
create droplet(server) on DigitalOcean
when its got created we net to setups Firewall rule. 
    name: my-firewall-nexus-test
    Inbound Rules :  Type-ssh 	Protocol:tcp 	PortRange:22 	Sources:IP_adddress of my laptop
    Apply to Droplets : select droplet on which we want to create rules
    Click on create firewall button

Conect to server through terminal
    Sadhanas-MacBook-Pro:workspace sanvi$ ssh root@159.89.126.81

    Install java 8
    root@ubuntu-s-2vcpu-4gb-tor1-01:~# apt install openjdk-8-jre-headless

    we needed version 8 for nexus
    root@ubuntu-s-2vcpu-4gb-tor1-01:~# java -version
        openjdk version "1.8.0_392"

    we need to move /opt folder. We need to install nexus package in opt folder.
    root@ubuntu-s-2vcpu-4gb-tor1-01:~# cd /opt/
                    Google Search "Nexus download sonatype" -> copy link on Unix Archive
    paste with wget command to download nexus
    root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# wget https://download.sonatype.com/nexus/3/nexus-3.64.0-04-unix.tar.gz

    $ls : we can see nexus tar file is there, next we need to do untar

    root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# tar -zxvf nexus-3.64.0-04-unix.tar.gz

    we can see 
    root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# ls
        digitalocean  nexus-3.64.0-04  nexus-3.64.0-04-unix.tar.gz  sonatype-work
        nexus : contains runtime and apllication of nexus
                root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# ls nexus-3.64.0-04    
                            NOTICE.txt  OSS-LICENSE.txt  PRO-LICENSE.txt  bin  deploy  etc  lib  public  replicator  system
        sonatype-work : contains own config for nexus and data
                root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# ls sonatype-work/nexus3/
                            clean_cache  log  tmp
                Subdirectories depending on your Nexus configuration
                    IP address that accessed Nexus
                    Logs of Nexus App
                    Your uploaded files and metadata
                    You can use this folder for backup

we should not start services as a root user for securities reasons
se we need to create nexus user
    root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# adduser nexus

Now we need to check the permissions for nexus and sonatype , we need to change ownership permission for user and group user as nexus
root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# ls -l
total 226284
drwxr-xr-x  4 root root      4096 Feb  5 19:39 digitalocean
drwxr-xr-x 10 root root      4096 Feb  5 20:10 nexus-3.64.0-04
-rw-r--r--  1 root root 231700148 Jan 12 16:28 nexus-3.64.0-04-unix.tar.gz
drwxr-xr-x  3 root root      4096 Feb  5 20:10 sonatype-work

root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# chown nexus:nexus nexus-3.64.0-04
root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# chown R nexus:nexus sonatype-work/
root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# ls -l
total 226284
drwxr-xr-x  4 root  root       4096 Feb  5 19:39 digitalocean
drwxr-xr-x 10 nexus nexus      4096 Feb  5 20:10 nexus-3.64.0-04
-rw-r--r--  1 root  root  231700148 Jan 12 16:28 nexus-3.64.0-04-unix.tar.gz
drwxr-xr-x  3 nexus nexus      4096 Feb  5 20:10 sonatype-work

Next step we need to setup Nexus configuration so it will run as a nexus user
    root@ubuntu-s-2vcpu-4gb-tor1-01:/opt# vim nexus-3.64.0-04/bin/nexus.rc 
        run_as_user="nexus"
    it will run as nexus user 
    now we can run services

Switch user 
    #su - nexus

Now we can start nexus service
    nexus@ubuntu-s-2vcpu-4gb-tor1-01:~$ /opt/nexus-3.64.0-04/bin/nexus start
        Starting nexus
we can check nexus service is running, PID
    @ubuntu-s-2vcpu-4gb-tor1-01:~$ ps aux | grep nexus
    
    [$netstat -lptn ==Command 'netstat' not found, but can be installed with:
                apt install net-tools
                Please ask your administrator., so I installed as a root user netstat   # apt install net-tools ]

    nexus@ubuntu-s-2vcpu-8gb-160gb-intel-tor1-01:~$ netstat -lptn
        check identify pid and port, 8081

        It accessible for externel request 8081port 

Next we need to setup firewall configuration for our droplet port 8081 to access nexus through bowser 
    inbound : Custom 	TCP 	8081 	All IPv4 All IPv6
Now we can acces through browser ip_droplet:nexus service port
    159.203.24.109:8081

we can login in nexus:
    when nexus is deployed default user created, we can get password
        root@ubuntu-s-2vcpu-8gb-160gb-intel-tor1-01:~# cat /opt/sonatype-work/nexus3/admin.password
    
    nexus is repository managaer, means we can have multiple repositories with different format
    like helm chart,docker images,javascript artifact, java archive
    types of repository : 
        poxy 
        group
        hosted


Nexus:
Create User: 
    Goto Security-> Create Local user Name:Sanvi , Satus :Active, Role :nx-Anonyms click on create button
    Now we need to make own nexus rule
Role:
    Type:Nexus role, Role Name -Role Name: nx-java, Privileges nx-repository-view-maven2-maven-snapshots-* click on save button
Now Role is created so we can assign our Role to created user.
Go to User -> click on created user as Sanvi and Define Role as nx-java click on Save button
    
